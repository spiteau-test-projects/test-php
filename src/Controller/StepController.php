<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\DefaultModel;
use App\Model\TemporalDataSet;
use App\Model\Step;
use App\Model\StepGenerator;

class StepController
{   
    public function index(): Response
    {
        $sourceList = [];
        
        $dataA = $this->loadSource('brand-a.json');
        $generator = $this->parseData($dataA);
        
        /* $dataB = $this->loadSource('brand-b.json');
         foreach($dataB as $source) {
            echo json_encode($source);die;
            $sourceList[$source['source']] = $this->parseData($source);
        } */
        
        return new JsonResponse($generator->getStepsCount(new \DateTime('2020-01-01'), new \DateTime('2020-12-31')));
    }
    
    private function loadSource(string $sourceFile) : array {
        $sourcePath = __DIR__ . '/../../var/data/';
        $data = file_get_contents($sourcePath . $sourceFile);
        return json_decode($data, true);
    }
    
    private function parseData($data) {
        $result = new TemporalDataSet();
        foreach($data['data'] as $stepData) {
            $result->addData(new Step($stepData));
        }
        return new StepGenerator($result);
    }
}