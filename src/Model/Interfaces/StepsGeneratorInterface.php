<?php
namespace App\Model\Interfaces;

use App\Model\TemporalDataSet;

interface StepsGeneratorInterface
{
    public function getTemporalDataSet(): TemporalDataSet;
    public function getStepsCount(\DateTime $from, \DateTime $to): int;
}
