<?php
namespace App\Model\Interfaces;

interface TemporalDataInterface
{
    public function getStart(): \DateTime;
    public function getEnd(): \DateTime;
    public function getValue();
}

