<?php
namespace App\Model;

use App\Model\Traits\ArrayFillTrait;
use App\Model\Interfaces\DateTime;
use App\Model\Interfaces\TemporalDataInterface;

class Step implements TemporalDataInterface {
    private \DateTime $start;
    private \DateTime $end;
    private int $value;
    
    public function __construct(iterable $data) {
        $this->start = new \DateTime();
        $this->end = new \DateTime();
        
        $this->start->setTimestamp($data['start']);
        $this->end->setTimestamp($data['end']);
        $this->value = $data['count'];
    }
    
    public function getValue()
    {
        return $this->value;
    }

    public function getStart(): \DateTime
    {
        return $this->start;
    }

    public function getEnd(): \DateTime
    {
        return $this->end;
    }

}
