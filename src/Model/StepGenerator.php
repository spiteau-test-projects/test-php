<?php
namespace App\Model;

use App\Model\Interfaces\StepsGeneratorInterface;

class StepGenerator implements StepsGeneratorInterface
{
    private TemporalDataSet $dataSet;
    
    public function __construct(TemporalDataSet $dataSet) {
        $this->dataSet = $dataSet;
    }
    
    public function getTemporalDataSet(): TemporalDataSet
    {
        return $this->dataSet;
    }

    public function getStepsCount(\DateTime $from, \DateTime $to): int
    {
        $result = 0;
        while ($this->dataSet->next() && $this->dataSet->valid()) {
            if ($this->dataSet->current()->getStart() > $from && $this->dataSet->current()->getEnd() < $to) {
                $result += $this->dataSet->current()->getValue();
            }
        }
        return $result;
    }
    
}

