<?php
namespace App\Model;

use App\Model\Interfaces\TemporalDataInterface;

class TemporalDataSet implements \Iterator
{
    private $position = 0;
    private $data = [];
    
    public function rewind()
    {
        $this->position = 0;
    }
    public function current()
    {
        return $this->data[$this->position];
    }
    public function key()
    {
        return $this->position;
    }
    public function next()
    {
        return ++$this->position;
    }
    public function valid()
    {
        return isset($this->data[$this->position]);
    }
    public function addData(TemporalDataInterface $temporalData)
    {
        $this->data[] = $temporalData;
    }
}
