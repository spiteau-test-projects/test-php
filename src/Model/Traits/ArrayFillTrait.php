<?php
namespace App\Model\Traits;

trait ArrayFillTrait
{
    private function loadData(iterable $data) {
        foreach($data as $attributeName => $attributeValue) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $attributeValue;
            }
        }
    }
}

